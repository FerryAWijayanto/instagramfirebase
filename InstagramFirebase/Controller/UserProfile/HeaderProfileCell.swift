//
//  HeaderProfileCell.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 13/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class HeaderProfileCell: UICollectionViewCell {
    
    var user: User? {
        didSet{
            fetchProfileImage()
            userLabel.text = user?.username
        }
    }
    
    let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 80 / 2
        iv.layer.masksToBounds = true
        iv.backgroundColor = .red
        return iv
    }()
    
    let userLabel: UILabel = {
        let label = UILabel()
        label.text = "Some Label"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let postLabel: UILabel = {
        let label = UILabel()
        let attributeText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        attributeText.append(NSMutableAttributedString(string: "posts", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        label.attributedText = attributeText
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let followerLabel: UILabel = {
        let label = UILabel()
        let attributeText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        attributeText.append(NSMutableAttributedString(string: "followers", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        label.attributedText = attributeText
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let followingLabel: UILabel = {
        let label = UILabel()
        let attributeText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        attributeText.append(NSMutableAttributedString(string: "following", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        label.attributedText = attributeText
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let editProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Edit Profile", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        return button
    }()
    
    let gridButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        return button
    }()
    
    let listButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.3)
        return button
    }()
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.3)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileImageView)
        profileImageView.anchor(top: safeAreaLayoutGuide.topAnchor, trailing: nil, bottom: nil, leading: safeAreaLayoutGuide.leadingAnchor, paddingTop: 15, paddingRight: 0, paddingBottom: 0, paddingLeft: 15, width: 80, height: 80)
        
        addSubview(userLabel)
        userLabel.anchor(top: profileImageView.bottomAnchor, trailing: nil, bottom: nil, leading: profileImageView.leadingAnchor, paddingTop: 18, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 0)
        
        setupUserStatView()
        setupToolbar()
        
        addSubview(editProfileButton)
        editProfileButton.anchor(top: postLabel.bottomAnchor, trailing: followingLabel.trailingAnchor, bottom: nil, leading: postLabel.leadingAnchor, paddingTop: 4, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 34)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUserStatView() {
        let stackView = UIStackView(arrangedSubviews: [postLabel, followerLabel, followingLabel])
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        stackView.anchor(top: safeAreaLayoutGuide.topAnchor, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: profileImageView.trailingAnchor, paddingTop: 10, paddingRight: 12, paddingBottom: 0, paddingLeft: 12, width: 0, height: 50)
    }
    
    fileprivate func setupToolbar() {
        let stackView = UIStackView(arrangedSubviews: [gridButton, listButton, bookmarkButton])
        stackView.distribution = .fillEqually
        
        let topDeviderLine = UIView()
        topDeviderLine.backgroundColor = UIColor.lightGray
        
        let bottomDeviderLine = UIView()
        bottomDeviderLine.backgroundColor = UIColor.lightGray
        
        addSubview(stackView)
        stackView.anchor(top: nil, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: bottomAnchor, leading: safeAreaLayoutGuide.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 50)
        
        addSubview(topDeviderLine)
        topDeviderLine.anchor(top: stackView.topAnchor, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: safeAreaLayoutGuide.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 0.3)
        
        addSubview(bottomDeviderLine)
        bottomDeviderLine.anchor(top: nil, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: stackView.bottomAnchor, leading: safeAreaLayoutGuide.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 0.3)
    }
    
    // Fetch User profile Image
    fileprivate func fetchProfileImage() {
        guard let profileImageUrl = user?.profileImageUrl else { return }
        guard let url = URL(string: profileImageUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                print("Failed to fetch profile image:", err)
            }
            
            guard let data = data else { return }
            
            let image = UIImage(data: data)
            
            DispatchQueue.main.async {
                self.profileImageView.image = image
            }
        }.resume()
    }
}













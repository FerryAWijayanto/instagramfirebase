//
//  SignUpViewController.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 10/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let photoPlusButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleImagePicker), for: .touchUpInside)
        return button
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
//    let signUpButton = FormButton(title: "Sign Up", image: nil, selector: #selector(handleSignUp), button: UIButton(type: .system), buttonEnable: false)
    
    let signInButton: UIButton = {
        let button = UIButton(type: .system)
        let attributeText = NSMutableAttributedString(string: "Already have an account? ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)])
        attributeText.append(NSMutableAttributedString(string: "Sign In", attributes: [NSAttributedStringKey.foregroundColor: UIColor.rgb(r: 17, g: 154, b: 237), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)]))
        button.setAttributedTitle(attributeText, for: .normal)
        button.addTarget(self, action: #selector(handleSignIn), for: .touchUpInside)
        return button
    }()
    
    let emailTextField: UITextField = {
        let tv = UITextField()
        tv.placeholder = "Email"
        tv.backgroundColor = .white
        tv.borderStyle = .roundedRect
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.addTarget(self, action: #selector(setupForm), for: .editingChanged)
        return tv
    }()
    
    let usernameTextField: UITextField = {
        let tv = UITextField()
        tv.placeholder = "Username"
        tv.backgroundColor = .white
        tv.borderStyle = .roundedRect
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.addTarget(self, action: #selector(setupForm), for: .editingChanged)
        return tv
    }()
    
    let passwordTextField: UITextField = {
        let tv = UITextField()
        tv.placeholder = "Password"
        tv.backgroundColor = .white
        tv.borderStyle = .roundedRect
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.addTarget(self, action: #selector(setupForm), for: .editingChanged)
        return tv
    }()
//    let emailTextField = FormTextField(formTextField: "Email", selector: #selector(setupForm))
//    let usernameTextField = FormTextField(formTextField: "Username", selector: #selector(setupForm))
//    let passwordTextField = FormTextField(formTextField: "Password", selector: #selector(setupForm))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupSignUp()
        
        view.addSubview(signInButton)
        signInButton.anchor(top: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 0)
    }
    
    @objc func handleSignIn() {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Create User, Store data in Database
    
    @objc func handleSignUp() {
        guard let email = emailTextField.text, email.count > 0 else {return }
        guard let password = passwordTextField.text, password.count > 0 else { return }
        guard let username = usernameTextField.text, username.count > 0 else { return }
        
        // Create user and format the source tree in firebase database
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let err = error {
                print("Failed to create a user:", err)
                return
            }
            
            print("Successfull create user:", user?.user ?? "")
            
            // Construct data in Firebase Storage
            guard let image = self.photoPlusButton.imageView?.image else { return }
            // Since image data not from url, this time the image date come from the photoPlusButton which is when we take from photo library
            guard let imageData = UIImageJPEGRepresentation(image, 0.3) else { return }
            
            let fileName = UUID().uuidString
            
            Storage.storage().reference().child("photo_image").child(fileName).putData(imageData, metadata: nil, completion: { (metadata, error) in
                if let err = error {
                    print("Failed to store image:", err)
                    return
                }
                
                // Metadata contain an URL
                metadata?.storageReference?.downloadURL(completion: { (url, error) in
                    if let err = error {
                        print("Failed to get url:", err)
                        return
                    }
                    
                    guard let profileImageUrl = url?.absoluteString else { return }
                    guard let uid = user?.user else { return }
                    let userDictionary = ["username": username, "profileImageUrl": profileImageUrl]
                    let value = [uid: userDictionary]
                    
                    Database.database().reference().child("users").updateChildValues(value, withCompletionBlock: { (error, reference) in
                        if let err = error {
                            print("Failed to save user data in database:", err)
                            return
                        }
                        print("Succesfully save user data in database")
                        
                        // Reset Sign in/Sign Up User causing BAD STATE like after log out or log in, the last user still have name
                        // in navigation title when a new user try to sign in
                        guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
                        
                        let layout = UICollectionViewFlowLayout()
                        let profileViewController = ProfileViewController(collectionViewLayout: layout)
                        
                        mainTabBarController.generateTabBarController(rootViewController: profileViewController, unselectedImage: #imageLiteral(resourceName: "profile_unselected"), selectedImage: #imageLiteral(resourceName: "profile_unselected"))
                        
                        // Dismiss after Sign in
                        self.dismiss(animated: true, completion: nil)
                    })
                })
            })
        }
    }
    
    @objc func setupForm() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0 && usernameTextField.text?.count ?? 0 > 0 && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = UIColor.rgb(r: 17, g: 154, b: 237)
        } else {
            signUpButton.isEnabled = false
            signUpButton.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        }
    }
    
    @objc func handleImagePicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[ "UIImagePickerControllerEditedImage"] as? UIImage {
            photoPlusButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            photoPlusButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        photoPlusButton.layer.cornerRadius = photoPlusButton.frame.width / 2
        photoPlusButton.layer.masksToBounds = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func setupSignUp() {
        view.addSubview(photoPlusButton)
        photoPlusButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, trailing: nil, bottom: nil, leading: nil, paddingTop: 20, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 140, height: 140)
        photoPlusButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signUpButton])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        view.addSubview(stackView)
        stackView.anchor(top: photoPlusButton.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: view.safeAreaLayoutGuide.leadingAnchor, paddingTop: 20, paddingRight: 20, paddingBottom: 0, paddingLeft: 20, width: 0, height: 190)
        
    }
}





















//
//  SignInController.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 15/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import Firebase

class SignInController: UIViewController {
    
    let containerView: UIView = {
        let view = UIView()
        
        let imageLogo = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
        imageLogo.contentMode = .scaleAspectFit
        
        view.addSubview(imageLogo)
        imageLogo.anchor(top: view.layoutMarginsGuide.topAnchor, trailing: nil, bottom: view.layoutMarginsGuide.bottomAnchor, leading: nil, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 200, height: 50)
        imageLogo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.backgroundColor = UIColor.rgb(r: 0, g: 120, b: 175)
        return view
    }()
    
    let emailTextField: UITextField = {
        let tv = UITextField()
        tv.placeholder = "Email"
        tv.backgroundColor = .white
        tv.borderStyle = .roundedRect
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.addTarget(self, action: #selector(setupForm), for: .editingChanged)
        return tv
    }()
    
    let passwordTextField: UITextField = {
        let tv = UITextField()
        tv.placeholder = "Password"
        tv.backgroundColor = .white
        tv.borderStyle = .roundedRect
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.addTarget(self, action: #selector(setupForm), for: .editingChanged)
        return tv
    }()
    
//    let emailTextField = FormTextField(formTextField: "Email", selector: #selector(setupForm))
//    let passwordTextField = FormTextField(formTextField: "Password", selector: #selector(setupForm))
    
//    let signInButton = FormButton(title: "Sign In", image: nil, selector: #selector(handleSignIn), button: UIButton(type: .system), buttonEnable: false)
    let signInButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign In", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleSignIn), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        let attributeText = NSMutableAttributedString(string: "Don't have an account? ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)])
        attributeText.append(NSMutableAttributedString(string: "Sign Up", attributes: [NSAttributedStringKey.foregroundColor: UIColor.rgb(r: 17, g: 154, b: 237), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)]))
        button.setAttributedTitle(attributeText, for: .normal)
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true

        view.addSubview(containerView)
        containerView.anchor(top: view.topAnchor, trailing: view.trailingAnchor, bottom: nil, leading: view.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 150)
        
        setupFormSignIn()

        view.addSubview(signUpButton)
        signUpButton.anchor(top: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 0, height: 0)
        
    }
    
    //MARK:- Change status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Check if textField not empty and enable/disable button
    
    @objc func setupForm() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0 && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            signInButton.isEnabled = true
            signInButton.backgroundColor = UIColor.rgb(r: 17, g: 154, b: 237)
        } else {
            signInButton.isEnabled = false
            signInButton.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        }
    }
    
    //MARK:- Handle Sign in With Firebase
    
    @objc func handleSignIn() {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let err = error {
                print("Failed to sign In:", err)
                return
            }
            // Reset Sign in/Sign Up User causing BAD STATE like after log out or log in, the last user still have name
            // in navigation title when a new user try to sign in
            guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
            
            let layout = UICollectionViewFlowLayout()
            let profileViewController = ProfileViewController(collectionViewLayout: layout)
            
            mainTabBarController.generateTabBarController(rootViewController: profileViewController, unselectedImage: #imageLiteral(resourceName: "profile_unselected"), selectedImage: #imageLiteral(resourceName: "profile_unselected"))
            
            // Dismiss after Sign in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Sign Up Form
    
    @objc func handleSignUp() {
        let signUpViewController = SignUpViewController()
        navigationController?.pushViewController(signUpViewController, animated: true)
    }
    
    fileprivate func setupFormSignIn() {
        let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, signInButton])
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        stackView.anchor(top: containerView.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: view.safeAreaLayoutGuide.leadingAnchor, paddingTop: 30, paddingRight: 20, paddingBottom: 0, paddingLeft: 20, width: 0, height: 140)
    }
}








//
//  CustomView.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 16/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class FormTextField: UITextField {
    
    let formTextField: String
    let selector: Selector
    
    init(formTextField: String, selector: Selector) {
        self.formTextField = formTextField
        self.selector = selector
        super.init(frame: .zero)
        self.placeholder = formTextField
        self.backgroundColor = .white
        self.borderStyle = .roundedRect
        self.font = UIFont.systemFont(ofSize: 14)
        self.addTarget(self, action: selector, for: .editingChanged)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

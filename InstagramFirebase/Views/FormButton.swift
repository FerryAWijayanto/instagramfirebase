//
//  FormButton.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 16/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class FormButton: UIButton {

    let title: String?
    let image: UIImage?
    let selector: Selector?
    let button: UIButton?
    let buttonEnable: Bool?

    init(title: String?, image: UIImage?, selector: Selector?, button: UIButton?, buttonEnable: Bool?) {
        self.title = title
        self.selector = selector
        self.button = button
        self.buttonEnable = buttonEnable
        self.image = image
        super.init(frame: .zero)
        self.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)

        guard let buttonEnable = buttonEnable else { return }
        self.isEnabled = buttonEnable
        self.setTitle(title, for: .normal)
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 5

        if let image = image {
        self.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        guard let selector = selector else { return }
        self.addTarget(self, action: selector, for: .touchUpInside)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

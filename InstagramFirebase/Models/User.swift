//
//  User.swift
//  InstagramFirebase
//
//  Created by Ferry Adi Wijayanto on 13/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

struct User {
    let username: String
    let profileImageUrl: String
    
    init(dictionary: [String: Any]) {
        username = dictionary["username"] as? String ?? ""
        profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
    }
}
